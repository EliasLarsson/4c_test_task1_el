//
//  ViewController.swift
//  4C_Test_Task1_EL
//
//  Created by Elias Larsson on 2021-04-22.
//

import UIKit

class ViewController: UIViewController {
    
    var maximumSpeed = 30
    var tresholdSpeed = 50
    var registeredSpeeds = [25, 29, 30, 49, 49, 25, 85, 25, 25, 29, 20]

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        print("the speed bump was activated " + String(speedBumpOnOff(maxSpeed: maximumSpeed, maxTreshold: tresholdSpeed, detectedSpeeds: registeredSpeeds)) + " times.")
        
            
        }
        
    }

func speedBumpOnOff(maxSpeed: Int, maxTreshold: Int, detectedSpeeds: Array<Int>) -> Int {
    
    var speedbumpActive = false
    var activations = 0
    
    for speed in detectedSpeeds{
        if (speed > 30 && speed < 50) {
            if (speedbumpActive == false){
                speedbumpActive = true
                activations = activations + 1
            }
        }
        else {
            if(speedbumpActive == true){
                speedbumpActive = false
            }
        }
    }
    return activations

}

